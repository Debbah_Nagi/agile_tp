# Agile_TP

## Organisation : gitlab

![](https://gitlab.com/Debbah_Nagi/agile_tp/-/raw/master/Doc/Capture_d_%C3%A9cran_de_2020-09-11_14-47-24.png)
![](https://gitlab.com/Debbah_Nagi/agile_tp/-/raw/master/Doc/Capture_d_%C3%A9cran_de_2020-09-11_14-47-38.png)
![](https://gitlab.com/Debbah_Nagi/agile_tp/-/raw/master/Doc/Capture_d_%C3%A9cran_de_2020-09-11_14-50-06.png)

## Implémentation d'un algorithme Blast appliqué à une banque de données

Sous format Python dans le cadre du TP Agile de M2 Bioinformatique, le but étant de reproduire les résultats d'un blast avec une banque de données correspondant à un protéome, en le comparant avec une protéine prise dans une espèce proche. 

### Diagramme de Gantt

Temps prévu par étapes
![gantt](https://gitlab.com/Debbah_Nagi/agile_tp/-/raw/master/BLASTp.PNG)

### Étape 1 - Done

Création de liste de 3 mots depuis un fichier Fasta, par parsing direct, et utilisation de liste. 

### Étape 2 - Done 

Création d'une base de donnée de position en utilsant les données extraites dans l'étape 1, sous forme de liste. 

### Étape 3 - (Almost) Done

Création d'un dictionnaire contenant les occurrences de chaque mots dans la base de données avec leur position

### Étape 4 

Comparaison des mots entre la banque de donnée et la protéine choisie, avec un score selon une occurrence identique ou non

### Étape 5 

Calcul des E-Values à partir du score obtenu en Étape 4 ainsi que la longueur de la séquence choisi et celle de la banque de données

### Étape 6 

Écriture des comparaisons obtenues avec la séquence de la banque de donnée, ainsi l'E-Value et l'ID de cette séquence. 

## Script

```python
#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Author : Debbah Nagi
"""

# Step 1
def list_word(name_file):
    list_words=[]
    list_seq = []
    list_len = []
    with open (name_file, "r") as seq:
        len_seq = 0
        for lines in seq:       
            if lines.startswith(">"):
                if len_seq !=0:
                    list_len.append(len_seq)
                len_seq = 0
                list_seq.append(lines[1:9])
            if not lines.startswith(">"):
                lines = lines.strip()
                for i in range(0, len(lines)): 
                    if i+3 > len(lines):
                        pass
                    else:
                        list_words.append(lines[i:i+3])                    
                        i += 1
                len_seq += len(lines)
            
        return list_words, list_seq, list_len
    
# Step 2

def database(list_words, list_seq, len_seq):
    position = []
    for i,j in zip (range(0, len(list_seq)), len_seq):
        for d in range(0, len(list_words)):
            if d + 2 <= j:
                position.append("{} {} {}".format(list_words[d], i, d))
            else:
                break
    
    return position

# Step 3

def hit(list_position):
    dict_hit = {}
    for position in list_position:
        pos = position.split()
        if pos[0] not in dict.keys():
            dict_hit[pos[0]] = "{}, {}".format(pos[1], pos[2])
        if pos[0] in dict.keys():
            dict_hit[pos[0]] += "{}, {}".format(pos[1], pos[2])
    return dict_hit
    

name_file = ("UP000002311_559292.fasta")

list_words, list_seq, list_len = list_word(name_file)

position = database (list_words,list_seq, list_len)
hits =  hit(position)
```
