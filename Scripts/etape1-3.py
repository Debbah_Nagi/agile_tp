#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Author : Debbah Nagi
"""

# Step 1
def list_word(name_file):
    list_words=[]
    list_seq = []
    list_len = []
    with open (name_file, "r") as seq:
        len_seq = 0
        for lines in seq:       
            if lines.startswith(">"):
                if len_seq !=0:
                    list_len.append(len_seq)
                len_seq = 0
                list_seq.append(lines[1:9])
            if not lines.startswith(">"):
                lines = lines.strip()
                for i in range(0, len(lines)): 
                    if i+3 > len(lines):
                        pass
                    else:
                        list_words.append(lines[i:i+3])                    
                        i += 1
                len_seq += len(lines)
            
        return list_words, list_seq, list_len
    
# Step 2

def database(list_words, list_seq, len_seq):
    position = []
    for i,j in zip (range(0, len(list_seq)), len_seq):
        for d in range(0, len(list_words)):
            if d + 2 <= j:
                position.append("{} {} {}".format(list_words[d], i, d))
            else:
                break
    
    return position

# Step 3

def hit(list_position):
    dict_hit = {}
    for position in list_position:
        pos = position.split()
        if pos[0] not in dict.keys():
            dict_hit[pos[0]] = "{}, {}".format(pos[1], pos[2])
        if pos[0] in dict.keys():
            dict_hit[pos[0]] += "{}, {}".format(pos[1], pos[2])
    return dict_hit
    

name_file = ("UP000002311_559292.fasta")

list_words, list_seq, list_len = list_word(name_file)

position = database (list_words,list_seq, list_len)
hits =  hit(position)